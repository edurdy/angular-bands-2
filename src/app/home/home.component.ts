// Imports

import { Component, OnInit } from '@angular/core';
import { Title, Meta }     from '@angular/platform-browser';

import { Band } from '../bands/bands';
import { BandService } from '../bands/bands.service';

// Component declaration

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit {

  bands: Band[] = [];

  constructor(
    private bandService: BandService,
    private titleService: Title,
    private metaService: Meta  
  ) { }

  ngOnInit(): void {

    // Get list of bands

    this.bandService.getBands()
      .then(bands => this.bands = bands);

    // Update open graph and metadata  
      
    this.titleService.setTitle( "Angular Rocks" );
    this.metaService.updateTag({content: "Esto es la descripción por defecto"},"name='description'");
    this.metaService.updateTag({content: "angular rocks default"},"name='keywords'");
    this.metaService.updateTag({content: "Esto es la descripción por defecto"},"property='og:description'");
    this.metaService.updateTag({content: "Angular Rocks"},"property='og:title'");

  }

}
