// Imports

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Title, Meta } from '@angular/platform-browser';

import { BandService } from '../bands/bands.service';
import { Band } from '../bands/bands';

import 'rxjs/add/operator/switchMap';

// Component declaration

@Component({
  selector: 'app-band-detail',
  templateUrl: './band-detail.component.html',
  styleUrls: ['./band-detail.component.scss']
})

export class BandDetailComponent implements OnInit {
  
  band: Band;
  showVideo: boolean = false;

  constructor(
    private bandService: BandService,
    private route: ActivatedRoute,
    private titleService: Title,
    private metaService: Meta
  ) { }

  ngOnInit(): void {

    // Get selected band

    this.route.paramMap
    .switchMap((params: ParamMap) => this.bandService.getBand(params.get('id')))
    .subscribe(band => {
        this.band = band;

        // Update open graph and metadata
        
        this.titleService.setTitle( this.band.name );
        this.metaService.updateTag({content: this.band.description},"name='description'");
        this.metaService.updateTag({content: this.band.keywords},"name='keywords'");
        this.metaService.updateTag({content: this.band.description},"property='og:description'");
        this.metaService.updateTag({content: this.band.name},"property='og:title'");
      });

  }

}
