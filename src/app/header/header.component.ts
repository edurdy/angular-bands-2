// Imports

import { Component, OnInit } from '@angular/core';

// Component declaration

@Component({
  selector: 'header-component',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})

export class HeaderComponent implements OnInit {

  title = 'Angular Rocks';

  constructor() { }

  ngOnInit(): void {
    
  }

}
