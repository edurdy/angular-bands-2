// Imports

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { BandDetailComponent } from './band-detail/band-detail.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { UnderConstructionComponent } from './under-construction/under-construction.component';

// Set routes

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'foo', component: UnderConstructionComponent },
  { path: 'bar', component: UnderConstructionComponent },
  { path: ':id', component: BandDetailComponent }
];

// Module declaration

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
