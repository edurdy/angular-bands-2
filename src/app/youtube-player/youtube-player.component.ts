// Imports

import { Component, OnInit, Input } from '@angular/core';
import { DomSanitizer, SafeResourceUrl, SafeUrl } from '@angular/platform-browser';

import { BandService } from '../bands/bands.service';
import { Band } from '../bands/bands';

// Component declaration

@Component({
  selector: 'app-youtube-player',
  templateUrl: './youtube-player.component.html',
  styleUrls: ['./youtube-player.component.scss']
})

export class YoutubePlayerComponent implements OnInit {

	@Input() id: string;
	public videoUrl:SafeResourceUrl;
	dangerousVideoUrl: string;

	constructor(private _sanitizer: DomSanitizer){
		
	} 

	ngOnInit() {

		// Get selected band video id

		this.updateVideoUrl(this.id);
	}

	updateVideoUrl(id: string) {

		// Sanitize video url

		this.dangerousVideoUrl = 'https://www.youtube.com/embed/' + id + '?autoplay=1';
		this.videoUrl =
			this._sanitizer.bypassSecurityTrustResourceUrl(this.dangerousVideoUrl);
	}

}
