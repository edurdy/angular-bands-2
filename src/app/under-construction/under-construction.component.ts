// Imports

import { Component, OnInit } from '@angular/core';
import { Title, Meta }     from '@angular/platform-browser';

// Component declaration

@Component({
  selector: 'app-under-construction',
  templateUrl: './under-construction.component.html',
  styleUrls: ['./under-construction.component.scss']
})

export class UnderConstructionComponent implements OnInit {

   constructor(
		private titleService: Title,
    	private metaService: Meta 
	) { }

	ngOnInit(): void {

		// Update open graph and metadata
		
	    this.titleService.setTitle( "Angular Rocks" );
	    this.metaService.updateTag({content: "Esto es la descripción por defecto"},"name='description'");
	    this.metaService.updateTag({content: "angular rocks default"},"name='keywords'");
	    this.metaService.updateTag({content: "Esto es la descripción por defecto"},"property='og:description'");
	    this.metaService.updateTag({content: "Angular Rocks"},"property='og:title'");

	}

}
