// Imports

import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { BandService } from './bands/bands.service';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BandDetailComponent } from './band-detail/band-detail.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { YoutubePlayerComponent } from './youtube-player/youtube-player.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { UnderConstructionComponent } from './under-construction/under-construction.component';

// Module declaration

@NgModule({
  declarations: [
    AppComponent,
    BandDetailComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    YoutubePlayerComponent,
    PageNotFoundComponent,
    UnderConstructionComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [
    Title, 
    [BandService]
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
