// Imports

import { Injectable } from '@angular/core';

import { Band } from './bands';
import { BANDS } from './mock-bands';

// Service declaration

@Injectable()
export class BandService {

	// Get list of bands

    getBands(): Promise<Band[]> {
        return Promise.resolve(BANDS);
    }

    // Get band selected

    getBand(id: string): Promise<Band> {
        return this.getBands()
                .then(bands => bands.find(band => band.id == id));
    }
}