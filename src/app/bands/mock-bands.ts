import { Band } from './bands';

export const BANDS: Band[] = [
 { 
 	id: 'the-rolling-stones', 
 	name: 'The Rolling Stones',
 	text: 'The Rolling Stones es una banda británica de rock originaria de Londres. La banda se fundó en abril de 19622​ por Brian Jones, Mick Jagger, Keith Richards, Bill Wyman, Ian Stewart y Charlie Watts.2​ Brian Jones fue despedido en junio de 1969, falleciendo tres semanas después, siendo reemplazado por el guitarrista Mick Taylor, que dejaría el grupo en 1975 y sería a su vez reemplazado por Ron Wood.',
 	video: 'byn7ZUjz5DY',
 	keywords: 'rolling stones londres mick jagger',
 	description: 'The Rolling Stones es una banda británica de rock originaria de Londres. La banda se fundó en abril de 1962​ por Brian Jones, Mick Jagger, Keith Richards, Bill Wyman, Ian Stewart y Charlie Watts.​' 
 },
 { 
 	id: 'the-beatles', 
 	name: 'The Beatles',
 	text: 'The Beatles fue una banda de pop/rock inglesa activa durante la década de 1960, y reconocida como la más exitosa comercialmente y la más alabada por la crítica en la historia de la música popular. Formada en Liverpool, estuvo constituida desde 1962 por John Lennon (guitarra rítmica, vocalista), Paul McCartney (bajo, vocalista), George Harrison (guitarra solista, vocalista) y Ringo Starr (batería, vocalista). ',
 	video: 'vefJAtG-ZKI',
 	keywords: 'beatles banda pop rock musica popular',
 	description: 'The Beatles fue una banda de pop/rock inglesa activa durante la década de 1960, y reconocida como la más exitosa comercialmente y la más alabada por la crítica en la historia de la música popular.' 
 },
 { 
 	id: 'queen', 
 	name: 'Queen',
 	text: 'Queen es una banda británica de rock formada en 1970 en Londres por el cantante Freddie Mercury, el guitarrista Brian May, el baterista Roger Taylor y el bajista John Deacon. Si bien el grupo ha presentado bajas de dos de sus miembros (Mercury, fallecido en 1991, y Deacon, retirado en 1997), los integrantes restantes, May y Taylor, continúan trabajando bajo el nombre Queen, por lo que la banda aún es considerada activa.',
 	video: 'fJ9rUzIMcZQ',
 	keywords: 'queen rock londres freddie mercury',
 	description: 'Queen es una banda británica de rock formada en 1970 en Londres por el cantante Freddie Mercury, el guitarrista Brian May, el baterista Roger Taylor y el bajista John Deacon.' 
 }
];