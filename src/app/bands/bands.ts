export class Band {
    id: string;
    name: string;
    description: string;
    text: string;
    video: string;
    keywords: string;
}